#include "../EFexEMAlgorithm.h"
#include "../EFexTauAlgorithm.h"
#include "../EFexEMEnergyWeightedClusterTool.h"
#include "../EFexEMClusterTool.h"

DECLARE_COMPONENT(LVL1::EFexEMAlgorithm)
DECLARE_COMPONENT(LVL1::EFexTauAlgorithm)
DECLARE_COMPONENT(LVL1::EFexEMEnergyWeightedClusterTool)
DECLARE_COMPONENT(LVL1::EFexEMClusterTool)
